FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

# This environment variable will be used by liveness/readiness probes to keep track of Apache configuration changes.
ENV RELOAD_TIMESTAMP_FILE=/etc/httpd/conf/last_reload

# For PHP 8.0 (core version), we need to install oci8-3.0.1: https://pecl.php.net/package/oci8.
ENV PHP_VERSION="8.0" \
    OCI_VERSION="3.0.1" \
    PHP_FPM_RUN_DIR=/run/php-fpm

# Available tags in https://github.com/coreruleset/coreruleset/tags
ENV WAF_CORERULESET_TAG="v4.7.0" \
    WAF_SHA256="b1402e3f4153a1b3c3bde68b90ff7bab96337b2ad29f861ae9557ff9a58a5c6f"

# Answer `yes` if CPAN asks to configure itself automatically.
ENV PERL_MM_USE_DEFAULT=1

# Packages we did NOT carry over from CC7:
# db4
# dbus-python
# gamin
# ghostscript-fonts
# gnome-vfs2
# libbonobo
# libbonoboui
# libc-client
# libcurl-openssl
# libgnome
# libgnomecanvas
# libgnomeui
# libXfont
# log4shib
# mod_auth_kerb
# opensaml-schemas
# perl-libxml-perl
# perl-Tie-DBI
# perl-XML-Grove
# php-imap
# php-pear-DB => MDB2 with pear
# php-pecl-ssh2
# python3-pysvn
# python-flup
# xapian-bindings-python
# xmltooling-schemas
# xorg-x11-font-utils
ENV INSTALL_PKGS="alsa-lib \
                apr \
                apr-util \
                audiofile \
                autoconf \
                automake \
                avahi \
                avahi-glib \
                cairo-devel\
                cronolog \
                cvs \
                cyrus-sasl-lib \
                dialog \
                elinks \
                expat \
                # Needed for php-fpm probes to connect directly to php-fpm
                fcgi \
                fontconfig \
                freetype \
                gd \
                gettext \
                ghostscript \
                giflib \
                glibc-devel \
                gnome-keyring \
                gnuplot \
                highlight \
                httpd \
                iftop \
                ImageMagick \
                imake \
                krb5-workstation \
                kstart \
                ld-linux.so.2 \
                libart_lgpl \
                libdb \
                libffi \
                libfontenc \
                libglade2 \
                libgsf \
                libmcrypt-devel\
                libnsl \
                libpng \
                librsvg2 \
                libstdc++ \
                libwmf \
                libX11 \
                libXau \
                libXdmcp \
                libXfont2 \
                libXpm \
                libxslt \
                libXt \
                lynx \
                mc \
                mod_auth_openidc \
                mod_perl \
                mod_security \
                mod_ssl \
                mysql \
                ncurses \
                netpbm \
                netpbm-progs \
                openssl \
                oracle-instantclient-basic \
                oracle-instantclient-devel \
                oracle-instantclient-sqlplus \
                oracle-instantclient-tnsnames.ora \
                pandoc \
                perl-CPAN \
                perl-CGI \
                perl-Date-Manip \
                perl-DBD-MySQL \
                perl-DBI \
                perl-Digest-HMAC \
                perl-Digest-SHA1 \
                perl-GD \
                perl-IO-Socket-SSL \
                perl-IO-String \
                perl-LDAP \
                perl-MailTools \
                perl-MIME-Lite \
                perl-Net-IP \
                perl-Net-SSLeay \
                perl-Parse-RecDescent \
                perl-SOAP-Lite \
                perl-Spreadsheet-XLSX \
                perl-SQL-Statement \
                perl-TermReadKey \
                perl-Test-NoWarnings \
                perl-Text-Template \
                perl-Unicode-String \
                perl-version \
                perl-WWW-Curl \
                perl-XML-Dumper \
                perl-XML-RegExp \
                perl-XML-Simple \
                perl-YAML \
                php \
                php-bcmath \
                php-dba \
                php-fpm \
                php-devel \
                php-gd \
                php-ldap \
                php-mbstring \
                php-mysqlnd \
                php-odbc \
                php-pdo \
                php-pear \
                php-pgsql \
                php-soap \
                php-xml \
                php-zip \
                pkg-config \
                pkgconfig \
                postgresql-libs \
                psutils \
                python3 \
                python3-dasbus \
                python3-dateutil \
                python3-devel \
                python3-ldap \
                python3-lxml \
                python3-numpy \
                python3-pip \
                python3-PyMySQL \
                python3-pytz \
                rcs \
                rlog \
                shared-mime-info \
                # Provides mailx
                s-nail \
                sqlite \
                tcl-xapian \
                ttmkfdir \
                unzip \
                urw-fonts \
                which \
                xapian-bindings \
                xapian-bindings-ruby \
                xapian-core \
                xapian-core-libs \
                zip"

# cx_Oracle, python-matplotlib and pycairo aren't available in Alma9.
# We're installing them with pip instead.
ENV INSTALL_PYTHON_PKGS="pip \
                        setuptools \
                        wheel \
                        cx_Oracle \
                        matplotlib \
                        pycairo"

# Copy extra files to the image.
COPY ./root.el9/ /

RUN dnf -y update && \
    dnf -y install epel-release && \
    dnf -y install $INSTALL_PKGS && \ 
    php -v | grep -qe "$PHP_VERSION\." && echo "Found PHP version $PHP_VERSION" && \
    dnf -y clean all --enablerepo='*' && \
    pecl update-channels && \
    ORACLE_HOME=$(cat /etc/ld.so.conf.d/oracle-instantclient.conf) && \
    # php-oci8 and php-mcrypt (used in CC7) aren't available in Alma9.
    # We're installing them with pecl instead.
    pecl install --configureoptions "with-oci8=\"instantclient,${ORACLE_HOME}\"" oci8-${OCI_VERSION} && \
    # We need to add oci8 in the default extension directory /etc/php.d
    echo $'; Enable oci8 extension module\nextension=oci8' > /etc/php.d/oci8.ini && \
    pecl install --configureoptions "with-mcrypt=\"true\"" mcrypt && \
    # We need to add mcrypt in the default extension directory /etc/php.d
    echo $'; Enable mcrypt extension module\nextension=mcrypt' > /etc/php.d/mcrypt.ini && \
    # MDB2 is an alternative of php-pear-DB (used in CC7).
    pear install MDB2 && \
    # perl-DBD-Oracle (used in CC7) isn't available in Alma9. We're installing it with cpan.
    cpan DBD::Oracle && \
    pip install --upgrade ${INSTALL_PYTHON_PKGS} && \
    # Due to https://gitlab.com/gitlab-org/gitlab-runner/issues/1736 we must make sure to `chmod`
    # all the files we COPY with the desired mode, or they will be world-writable by default.
    # - Make sure `/etc/httpd` is only readable by root since we need to store OIDC secrets in conf files.
    # - Files in /var/www/html and /etc/yum.repos.d/ should be world-readable (error pages).
    # - Initialize last_reload timestamp file.
    mkdir -p ${PHP_FPM_RUN_DIR} &&\
    chmod -R 700 /etc/httpd ${PHP_FPM_RUN_DIR} && \
    chmod -R 755 /var/www/html /etc/yum.repos.d/ && \
    touch ${RELOAD_TIMESTAMP_FILE} && \
    # Download and verify OWASP CRS 
    wget https://github.com/coreruleset/coreruleset/archive/refs/tags/${WAF_CORERULESET_TAG}.tar.gz && \
    echo "${WAF_SHA256}  ${WAF_CORERULESET_TAG}.tar.gz" | sha256sum -c - && \
    # extract files
    mkdir /etc/crs4 && \
    tar -xzvf ${WAF_CORERULESET_TAG}.tar.gz --strip-components 1 -C /etc/crs4 && \
    rm ${WAF_CORERULESET_TAG}.tar.gz && \
    # set up main configuration file
    mv /etc/crs4/crs-setup.conf.example /etc/crs4/crs-setup.conf

EXPOSE 8080
